# Learning Docker - LinkedIn Learning Course - part of Developing and Delivering Software with Docker

## Installing Docker

Fairly simple on Ubuntu.

https://docs.docker.com/engine/install/ubuntu/

Dont forget the extra steps:

https://docs.docker.com/engine/install/linux-postinstall/


## Using Docker

Image -> `docker run` -> Container 

Images are read only, and containers only have a writeable layer on top which lives the duration of the container

Running Container with some valuable files -> `docker commit <container_id> <image_name>`

Running processes inside docker containers:

`docker run -ti <image:tag> <process_to_run>`

:warning: A container only has one main process. Once that exists, the container will exit too.

:warning: When a container exits, it is still there, and can be found `docker ps -a`

(?) Useful command `docker run -ti --rm <image> bash -c ""` will run a series of commands after -c as if a bash script, and will remove the container after exit.

Detached running container `docker run -ti -d <image> bash`

To attach to a detached container `docker attach <container_name>`

Control + p, Control + q will detach from an attached container

Find running containers with `docker ps`

Running more things in a running container

`docker exec -ti <container_name> <process>`

### Looking at container output

`docker logs <container_name>`

### Stopping and removing containers

`docker kill <container_name>` -> ???

`docker rm <container_name`

### Managing resources

You can limit the resources of what the containers use

### Lessons from the field

:warning: Dont let containers fetch dependencies when they start 

:warning: Dont leave important work in unnamed stopped containers

### Exposing Ports

`docker run --rm -ti -p 45678:45678 -p 45679:45679 --name echo-server ubuntu:14.04 bash`

`nc -lp 45678 | nc -lp 45679` -> receive on one port and flow it into another port

`docker run --rm -ti -p 45678 --name echo-server ubuntu:14.04 bash`

Without specifying an outside port, docker will assign a random one

Finding port mappings for containers:

`docker port <container_name>`

### Container networking

`docker network ls` -> show all available networks


```
NETWORK ID     NAME              DRIVER    SCOPE
c0fff0c9d7c2   bridge            bridge    local
847440644ad9   host              host      local
cd926df7ecb4   none              null      local

```
none -> no networking
host -> no network isolation
bridge -> default

`docker network create <network_name>`

`docker run --rm -ti --net <network_name> --name <container_name> ...`

Putting containers on the same server allows them to access each other by name

`docker network connect <network_name> <container_name>` -> allows connecting a container to a network

### Legacy Linking

Lets not use it

### Listing Images

`docker images`

Cleaning up

`docker rmi <image_name>` -> removes images

### Volumes

Allows sharing of data between host & container and container-container

Two types:

- ephemeral -> will disapear when no container is using them
- persistent -> these stay

`docker run -ti -v <full_local_path:full_container_path> ...`

Sharing between containers

`docker run -ti -v <no_local_path>`

`docker run -ti --volumes-from <container_name> ....` -> connect to a volume in another container

### Docker registries

Managing and distrbuting images

Can run your own, or use dockers

:warning: Clean up your images regularly

# Building Images

Simple example

`docker build -t <desired_image_name> .` -> build the image from the docker file from the PWD, and give it a name

Once the build finishes, the image is in the local registry

Build happens in steps -> start with an image -> run the container -> do the commands -> make an image -> do the next step

:warning: large files should not span several layers

It caches steps, if a single layer is, unchanged

:warning: Put things you change the most at the end of the docker file, so image building is fast

:warning: Layers do not persist running proceses

### Example Dockerfile

```Dockerfile
# beggining comments
FROM debian:sid

RUN apt-get -y update

RUN apt-get install nano

CMD ["bin/nano", "/tmp/notes"]

```

Build an image with nano installed and opening a notes file on run

:warning: Longer commands are better if they are shell scripts copied and executed

### Dockerfile syntax

FROM -> specify what image to start from

LABEL -> provide metadata

RUN -> run a command in shell

ADD -> great for copying data into the image, !!! will automatically uncompress files & works with URLs

ENV -> set the environment variables, will stay in the resulting image

ENTRYPOINT -> beggining of the expression (entrypoint)

CMD -> command or after the entrypoint (command gets replaced)

EXPOSE -> expose ports in the container

VOLUME -> defines volumes, ephemeral or persistent 

WORKDIR -> sets the directory for the remaining of the run

USER -> change to a user

https://docs.docker.com/engine/reference/builder/

### Multistage builds

```Dockerfile
FROM ubuntu 16:04 as builder

RUN apt-get update

RUN apt-get -y install curl

RUN curl https://google.com | wc -c > google-size


FROM alpine

COPY --from=builder /google-size /google-size

ENTRYPOINT  echo google is this big; cat google-size

```

### Preventing golden images

i.e. Unmaintainable images

- Include installers in your project, whatever dependency you need, it might be gone later on

- Have a canonical system that builds your images from scratch

- Tag your builds with the git has of the code that built it

- Use small base images, like Alpine

- Build images from Dockerfiles, not from containers










